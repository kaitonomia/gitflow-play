# DevOps with Git

[Trunk-based development](https://www.atlassian.com/continuous-delivery/continuous-integration/trunk-based-development)

Forgot this one.

Content added by another developer.

## Workflow

1. Checkout origin main
1. Pull upstream main, or rebase upstream main
1. Checkout temp branch
1. Develop in temp branch
1. Commit and checking work to remote origin branch
1. When ready to create merge/pull request
    1. Pull latest changes from upstream main
    1. Push merged code to origin temp branch
1. Push code to merge to origin
1. Create merge/pull request
1. Keep working in temp branch till accepted or rejected
1. Checkout main
1. Pull origin main, or rebase origin main



